package eu.llit.bananenrepublik.model;

public class BestellungInfo {
	public int id;
	public int totalPrice;

	public BestellungInfo(int _id, int _totalPrice){
		id = _id;
		totalPrice = _totalPrice;
	}

}
