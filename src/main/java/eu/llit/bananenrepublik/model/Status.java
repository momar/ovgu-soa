package eu.llit.bananenrepublik.model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import eu.llit.bananenrepublik.Database;

public class Status {

    public int count;
    public int pricePerBanana;
    public int priceTotal;
    public String status;

    public Status() {
    } // used for testing

    public Status(Integer id) throws Exception {
        PreparedStatement prep;
        
        prep = Database.get().prepareStatement("SELECT count, priceTotal, status FROM Bestellung WHERE id = ?");
        prep.setInt(1, id);
        ResultSet result = prep.executeQuery();
        if (!result.next()) throw new NotFoundError("Keine Bestellung mit dieser Bestellnummer.");
        count = result.getInt(1);
        priceTotal = result.getInt(2);
        pricePerBanana = priceTotal / count;
        status = result.getString(3);
        prep.close();

        // Progress status by 1 every time the API is called.
        String newStatus = status;
        switch (status) {
            case "awaiting-payment": newStatus = "growing"; break;
            case "growing": newStatus = "packing"; break;
            case "packing": newStatus = "shipping"; break;
            case "shipping": newStatus = "delivered"; break;
            case "delivered": newStatus = Math.random() > 0.2 ? "eaten" : "spoiled"; break;
        }
        if (status != newStatus) {
            prep = Database.get().prepareStatement("UPDATE Bestellung SET status = ? WHERE id = ?;");
            prep.setString(1, newStatus);
            prep.setInt(2, id);
            prep.execute(); prep.close();
        }
    }
}
