package eu.llit.bananenrepublik.model;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import eu.llit.bananenrepublik.Database;

public class Review {
    public float rating;
    public String content;

    public Review(float _rating, String _content) {
        rating = _rating;
        content = _content;
    }

    public void publish() throws SQLException {
        if (this.rating < 0 || this.rating > 1) throw new RequestError("Bewertung muss zwischen 0 und 1 liegen!");
        PreparedStatement prep = Database.get().prepareStatement("INSERT INTO review VALUES (?, ?)");
        prep.setFloat(1, this.rating);
        prep.setString(2, this.content);
        prep.execute(); prep.close();
    }

    public boolean equals(Review o) {
        return this.rating == o.rating && this.content == o.content;
    }
}
