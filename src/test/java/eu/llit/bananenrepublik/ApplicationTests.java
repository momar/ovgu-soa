package eu.llit.bananenrepublik;

import org.apache.http.HttpStatus;
import eu.llit.bananenrepublik.model.Banane;
import eu.llit.bananenrepublik.model.Bestellung;
import eu.llit.bananenrepublik.model.NotFoundError;
import eu.llit.bananenrepublik.model.PaymentError;
import eu.llit.bananenrepublik.model.Review;
import eu.llit.bananenrepublik.model.Status;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import io.restassured.http.ContentType;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.equalTo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static io.restassured.RestAssured.given;

@RunWith(SpringRunner.class)
@SpringBootTest(
	classes = Application.class,
	webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT,
	properties = {"server.port=8080"}
)
public class ApplicationTests {

	@Test
	public void bananeIstBanane() {
		given()
		.when()
			.get("http://localhost:8080/v1/banane")
		.then()
			.statusCode(HttpStatus.SC_OK)
			.contentType(ContentType.JSON)
			.assertThat().equals(new Banane());
	}

	@Test
	public void bananeIstGuteBanane() throws Exception {
		List<Review> reviews = given()
		.when()
			.get("http://localhost:8080/v1/banane")
		.then()
			.statusCode(HttpStatus.SC_OK)
			.contentType(ContentType.JSON)
			.extract().body().path("reviews");
		
		if (reviews == null) reviews = new ArrayList<Review>();
		Review r = new Review(0.9f, "Super Banane, gerne wieder.");
		reviews.add(r);

		given()
			.contentType(ContentType.JSON)
			.body(r)
		.when()
			.post("http://localhost:8080/v1/banane")
		.then()
			.statusCode(HttpStatus.SC_OK);
		
		HashMap<String, Object> m = new HashMap<String, Object>();
		m.put("rating", r.rating);
		m.put("content", r.content);
		given()
		.when()
			.get("http://localhost:8080/v1/banane")
		.then()
			.statusCode(HttpStatus.SC_OK)
			.contentType(ContentType.JSON)
			.body("reviews", hasSize(reviews.size()))
			.and().body("reviews", hasItem(equalTo(m)));;
	}

	@Test
	public void kreditkarteIstBanane() {
		Bestellung b = new Bestellung();
		b.count = 5;
		b.address = "Max Mustermann\nMusterstr. 123a\n12345 Musterstadt";
		b.ccNumber = "0000000000000000";
		b.ccName = "Max Mustermann";
		b.ccValidity = 202307;
		b.ccCVC = 123;
		given()
			.contentType(ContentType.JSON)
			.body(b)
		.when()
			.post("http://localhost:8080/v1/bestellung")
		.then()
			.statusCode(HttpStatus.SC_PAYMENT_REQUIRED)
			.contentType(ContentType.JSON)
			.assertThat().equals(new PaymentError("Ihre Kreditkarte ist nicht für Bananen freigeschaltet."));
	}

	@Test
	public void bestellung() throws Exception {
		Bestellung b = new Bestellung();
		b.count = 5;
		b.address = "Max Mustermann\nMusterstr. 123a\n12345 Musterstadt";
		b.ccNumber = "4242424242424242";
		b.ccName = "Max Mustermann";
		b.ccValidity = 202307;
		b.ccCVC = 123;
		given()
			.contentType(ContentType.JSON)
			.body(b)
		.when()
			.post("http://localhost:8080/v1/bestellung")
		.then()
			.statusCode(HttpStatus.SC_OK)
			.contentType(ContentType.JSON)
			.assertThat().body("totalPrice", equalTo(new Banane().price * b.count));
	}


	@Test
	public void bestellstatus() {
		Bestellung b = new Bestellung();
		b.count = 5;
		b.address = "Max Mustermann\nMusterstr. 123a\n12345 Musterstadt";
		b.ccNumber = "4242424242424242";
		b.ccName = "Max Mustermann";
		b.ccValidity = 202307;
		b.ccCVC = 123;
		int order = given()
			.contentType(ContentType.JSON)
			.body(b)
		.when()
			.post("http://localhost:8080/v1/bestellung")
		.then()
			.statusCode(HttpStatus.SC_OK)
			.contentType(ContentType.JSON)
			.extract().body().path("id");
		
		Status s = new Status();
		s.count = 5;
		s.pricePerBanana = 500;
		s.priceTotal = 2500;
		s.status = "awaiting-payment";
		given()
		.when()
			.get("http://localhost:8080/v1/bestellung/" + Integer.toString(order))
		.then()
			.statusCode(HttpStatus.SC_OK)
			.contentType(ContentType.JSON)
			.assertThat().equals(s);
		
		given()
		.when()
			.get("http://localhost:8080/v1/bestellung/" + Integer.toString(order + 1))
		.then()
			.statusCode(HttpStatus.SC_NOT_FOUND)
			.contentType(ContentType.JSON)
			.assertThat().equals(new NotFoundError("Keine Bestellung mit dieser Bestellnummer."));
	}

}
