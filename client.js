const fetch = (typeof window !== "undefined") ? window.fetch : require("node-fetch");
const banana = {
    prefix: "http://localhost:8080/v1",
    async handleError(r) {
        if (!r.ok && r.headers.get("Content-Type").indexOf("application/json") > -1) { const j = await r.json(); throw j.error ? new Error(j.error) : j; }
        if (!r.ok && r.headers.get("Content-Type")) throw new Error(await r.text());
        if (!r.ok) throw new Error(r.status + " " + r.statusText);
    },
    async info() {
        const r = await fetch(banana.prefix + "/banane");
        await banana.handleError(r);
        return await r.json();
    },
    async review(rating, content) {
        const r = await fetch(banana.prefix + "/banane", {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({
                rating,
                content,
            }),
        });
        await banana.handleError(r);
        return await r.json();
    },
    async order(count, address, cc) {
        const r = await fetch(banana.prefix + "/bestellung", {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({
                count,
                address: address || "Otto-von-Guericke-Universität Magdeburg\nUniversitätsplatz 2\n39106 Magdeburg",
                ccNumber: cc ? cc.number : "",
                ccName: cc ? cc.name : "",
                ccValidity: cc ? cc.validity : 0,
                ccCVC: cc ? cc.cvc : 0,
            }),
        });
        await banana.handleError(r);
        return await r.json();
    },
    async status(id) {
        const r = await fetch(banana.prefix + "/bestellung/" + id);
        await banana.handleError(r);
        return await r.json();
    },
}

if (typeof window !== "undefined") window.banana = banana;
else global.banana = banana;
