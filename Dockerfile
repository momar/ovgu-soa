FROM openjdk:8
ADD . /build
RUN cd /build && chmod +x mvnw && ./mvnw package && mkdir /data
WORKDIR /data
EXPOSE 8080
CMD ["java", "-jar", "/build/target/bananenrepublik-1.0.0.jar"]
# docker build -t api2b .
# docker run --rm -p 8080:8080 api2b  # Add "-v $PWD:/data" before "api2b" to make the database persistent
